//
//  BCRecognize.h
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 26/02/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//
@import UIKit;
@import Foundation;

@interface BCRecognizer : NSObject

- (NSString *)barcodeFromImage:(UIImage *)image;

@end
