//
//  Product.h
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 01/03/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (nonnull, nonatomic, strong)  NSString *alccode;
@property (nonnull, nonatomic, strong)  NSString *fullname;
@property (nonnull, nonatomic, strong)  NSDate *addedDate;
@property (nonnull, nonatomic, strong)  NSString *cachedJson;
@property (nullable, nonatomic, strong) NSDictionary *cachedJsonDictionary;
@property (nonatomic, assign) BOOL isFound;

//TODO: 1) метод для парсинга
//TODO: 2) метод для сохранения в CD
/*
 
 */

+ (_Nonnull instancetype)productWithAlccode:( NSString * _Nonnull)alccode;
+ (_Nonnull instancetype)productWithAlccode:( NSString * _Nonnull)alccode andDictionary:(NSDictionary * _Nullable)dict;

@end
