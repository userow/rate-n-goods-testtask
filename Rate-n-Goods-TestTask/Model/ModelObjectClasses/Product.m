//
//  Product.m
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 01/03/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "Product.h"

@implementation Product


/*
 {
 "status": "Ok!",
 "errorCode": 0,
 "data": {
 "id": "201332",
 "producer_fsrarid": "030000232087",
 "importer_fsrarid": "",
 "alccode": "0363137000001288949",
 "fullname": "Водка "СТАРАЯ МАРКА КЛАССИЧЕСКАЯ"",
 "capacity": "0.5",
 "alcvolume": "40",
 "vcode": "200",
 "refresh_date": "2016-10-05 12:44:17",
 "producer": {
 "id": "58778",
 "fsrarid": "030000232087",
 "inn": "6318238581",
 "kpp": "631801001",
 "fullname": "Общество с ограниченной ответственностью Самарский комбинат "Родник"",
 "shortname": "ООО СК "Родник"",
 "address": "РОССИЯ,,САМАРСКАЯ ОБЛ,,Самара г,,Ветлянская ул, д. 50,,",
 "country": "643",
 "region": "63",
 "refresh_date": "2017-02-28 11:39:00"
 },
 "importer": null
 }
 }
 */

+ (instancetype)productWithAlccode:(NSString*)alccode {
    Product *prod = [Product new];
    
    if (prod) {
        prod.alccode = alccode;
        prod.addedDate = [NSDate date];
    }
    
    return prod;
}

+ (instancetype)productWithAlccode:(NSString*)alccode andDictionary:(NSDictionary *)dict;
{
    //проинитили, записали timestamp и код
    Product *prod = [Product productWithAlccode:alccode];
    
    //Parsing - сохраняем словарик.
    prod.cachedJsonDictionary = dict;
    
    NSError *err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&err];
    
    if (!err) {
        NSString * myString = [[NSString alloc] initWithData:jsonData
                                                    encoding:NSUTF8StringEncoding];
        prod.cachedJson = myString;
    }
    
    //Parsing -  статус (найден / нет)
    if (![dict[@"status"] isEqualToString:@"Ok!"]) {
        [prod setIsFound:NO];
    } else {
        [prod setIsFound:YES];
    
        //Parsing - наименование
        prod.fullname = dict[@"fullname"];
    }
    
    return prod;
}

- (void)setIsFound:(BOOL)isFound {
    _isFound = isFound;
    if (!isFound) {
        _fullname = @"не найден";
    }
}

//TODO: Save to CoreData


//TODO: Load from CoreData


//TODO: adpter method - data source for ProdDetailTVC


@end
