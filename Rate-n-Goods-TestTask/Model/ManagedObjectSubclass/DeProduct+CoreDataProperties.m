//
//  DeProduct+CoreDataProperties.m
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 01/03/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "DeProduct+CoreDataProperties.h"

@implementation DeProduct (CoreDataProperties)

+ (NSFetchRequest<DeProduct *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"DeProduct"];
}

@dynamic addedDate;
@dynamic alccode;
@dynamic cachedJson;
@dynamic fullname;
@dynamic status;

@end
