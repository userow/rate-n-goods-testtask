//
//  DeProduct+CoreDataProperties.h
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 01/03/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "DeProduct+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface DeProduct (CoreDataProperties)

+ (NSFetchRequest<DeProduct *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *addedDate;
@property (nullable, nonatomic, copy) NSString *alccode;
@property (nullable, nonatomic, copy) NSString *cachedJson;
@property (nullable, nonatomic, copy) NSString *fullname;
@property (nonatomic) BOOL status;

@end

NS_ASSUME_NONNULL_END
