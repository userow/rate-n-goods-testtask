//
//  DeProduct+CoreDataClass.h
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 01/03/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeProduct : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "DeProduct+CoreDataProperties.h"
