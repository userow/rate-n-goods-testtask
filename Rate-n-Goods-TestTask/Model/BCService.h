//
//  BCService.h
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 28/02/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

@class BCApiClient;
@class Product;

#import <Foundation/Foundation.h>

typedef void (^ApiCallback)(Product *product);

@interface BCService : NSObject

@property (nonatomic, strong) BCApiClient *apiClient;

#pragma mark - singltone
+ (BCService *)sharedInstance;

+(instancetype) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
-(instancetype) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+(instancetype) new __attribute__((unavailable("new not available, call sharedInstance instead")));
-(instancetype) copy __attribute__((unavailable("copy not available, call sharedInstance instead")));

#pragma mark - methods

- (void)getAlccodeInfo:(NSString *)alccode withCallback:(ApiCallback)callback;

@end
