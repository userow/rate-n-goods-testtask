//
//  BCRecognize.m
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 26/02/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//


#import "BCRecognizer.h"

@implementation BCRecognizer


+ (NSArray *)barcodes
{
    static NSArray *_barcodes;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _barcodes = @[
                      @"0363137000001288949",
                      @"0015545077771866971",
                      @"0150368000003989279",
                      @"0150367000001193680",
                      @"0177272777773152636",
                      @"0350191000005524047",
                      @"0150367000002926402",
                      @"0377167777771398179",
                      @"0377160000001398179",
                      @"0177442000002175194",
                      @"0177504000002241453",
                      @"0036813000001379241",
                      @"0012624777772019492",
                      @"0015545000001866971",
                      @"0150357777771188485"
                      ];
    });
    return _barcodes;
}


- (NSString *)barcodeFromImage:(UIImage *)image;
{
    NSArray *fakeBarcodes = [[self class] barcodes];
    
    
    uint bcIdx = arc4random_uniform((uint)fakeBarcodes.count);
    
    NSString *bc = fakeBarcodes[bcIdx];
    
    NSLog(@"Barcode 'recognized': %@", bc);
    
    return bc;
}

@end
