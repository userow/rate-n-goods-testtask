//
//  BCApi.m
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 28/02/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BCApiClient.h"

static NSString * const egaisikAPIBaseURLString = @"http://xn--80affoam1c.xn--p1ai/api/";

@implementation BCApiClient


- (instancetype)init {
    if (self = [super initWithBaseURL:[NSURL URLWithString:egaisikAPIBaseURLString]]) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}

- (void)getProductInfoAlccode:(NSString*)alccode
                      success:(ApiSuccessBlock)success
                      failure:(ApiFailureBlock)fail;
{
//    NSDictionary *parameters = @{
//                                 @"login"    : @"demo",
//                                 @"pass"    : @"demo",
//                                 @"alccode" : alccode
//                                 };
    
    [self GET:[NSString stringWithFormat:@"product?login=demo&pass=demo&alccode=%@", alccode]
   parameters:nil//parameters
     progress:nil
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          NSLog(@"API - data received -\n\n%@", responseObject);
          success(task, responseObject);
      }
      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          NSLog(@"API - ERROR - \n\n%@", error);
          fail(task, error);
      }];
}

@end
