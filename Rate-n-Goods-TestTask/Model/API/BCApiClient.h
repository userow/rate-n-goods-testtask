//
//  BCApi.h
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 28/02/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

@import Foundation;
#import <AFNetworking.h>

typedef void (^ApiSuccessBlock)(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject);
typedef void (^ApiFailureBlock)(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull  error);

@interface BCApiClient : AFHTTPSessionManager

- (void)getProductInfoAlccode:(NSString  * _Nonnull)alccode
                      success:(_Nullable ApiSuccessBlock)success
                      failure:(_Nullable ApiFailureBlock)fail;

@end
