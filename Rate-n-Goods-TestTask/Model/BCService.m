//
//  BCService.m
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 28/02/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BCService.h"

#import "BCApiClient.h"
#import "Product.h"

@implementation BCService

#pragma mark - singletone

+ (BCService *)sharedInstance;
{
    static dispatch_once_t onceToken;
    static BCService *sharedInstance;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super alloc] initUniqueInstance];
    });
    
    return sharedInstance;
}

-(BCService *) initUniqueInstance {
    _apiClient = [BCApiClient new];
    
    return [super init];
}

#pragma mark - Api interaction

- (void)getAlccodeInfo:(NSString *)alccode withCallback:(ApiCallback)callback;
{
    [self.apiClient getProductInfoAlccode:alccode
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      
                                      if ([responseObject isKindOfClass:[NSDictionary class]]) {
                                          Product *prod = [Product productWithAlccode:[alccode copy]
                                                                        andDictionary:(NSDictionary *)responseObject];
                                          callback(prod);
                                      }
                                  }
                                  failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
                                      Product *prod = [Product productWithAlccode:[alccode copy]];
                                      prod.isFound = NO;
                                      
                                      callback(prod);
                                  }];
}

#pragma mark - CD interaction

//- () 

@end
