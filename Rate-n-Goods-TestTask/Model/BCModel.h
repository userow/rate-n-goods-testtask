//
//  BCModel.h
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 26/02/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

@import Foundation;
@import CoreData;

@interface BCModel : NSObject

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;

@end
