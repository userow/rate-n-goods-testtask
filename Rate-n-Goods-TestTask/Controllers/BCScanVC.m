//
//  FirstViewController.m
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 24/02/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BCScanVC.h"
@import AVFoundation;
@import CoreMedia;
@import CoreVideo;
@import ImageIO;

#import "BCRecognizer.h"
#import "BCProductDetailsTVC.h"
#import "Product.h"

#import "BCService.h"
#import "BCApiClient.h"

@interface BCScanVC () <UITextFieldDelegate> {
}

//IBOutlets
@property (weak, nonatomic) IBOutlet UITextField *barcodeText;
@property (weak, nonatomic) IBOutlet UIImageView *barcodeCapturedImageView;
@property (weak, nonatomic) IBOutlet UIView *preView;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;

@property (weak, nonatomic) IBOutlet UILabel *accessDeniedLabel;

//Properties
@property (nonatomic, strong) NSString *textEntered;

//AVFoundation related properties
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureDevice *captureDevice;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property (nonatomic, strong) AVCaptureDeviceInput *captureDeviceInput;
@property (nonatomic, strong) AVCaptureStillImageOutput *captureStillImageOutput;

@end

@implementation BCScanVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _textEntered = @"";
    
    self.barcodeCapturedImageView.backgroundColor = [UIColor lightGrayColor];
    
    self.accessDeniedLabel.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    //конфигурируем всё
    [self startPreview];
}

- (void)viewDidAppear:(BOOL)animated {
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if (authStatus == AVAuthorizationStatusAuthorized) {
        
        //стартуем сессию видеозахвата
        [_captureSession startRunning];
    } else if (authStatus == AVAuthorizationStatusDenied || authStatus == AVAuthorizationStatusRestricted) {
        
        //показываем, что доступ запрещён
        [self initializeDeniedPreview];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    //останавливаем сессию видеозахвата
    [_captureSession stopRunning];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Internal AVF stuff

- (void) startPreview
{
    if (!_captureSession) {
        _captureSession = [AVCaptureSession new];
        _captureSession.sessionPreset = AVCaptureSessionPresetMedium;
    }
    
    if (!_captureDevice) {
        NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
        
        //ищём все CaptureDevice
        for (AVCaptureDevice *dev in devices) {
            if (dev.position == AVCaptureDevicePositionBack) {
                _captureDevice = dev;
                break;
            }
        }
    }
    
    //тут идёт проверка прав доступа на камеру
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];

    if (authStatus == AVAuthorizationStatusNotDetermined) {
        
        //запрос ещё не дан, продёргиваем доступ
        
        __weak typeof(self) weakSelf = self;
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo
                                 completionHandler:^(BOOL granted) {
                                     if (granted) {
                                         dispatch_async(dispatch_get_main_queue(), ^{
                                             [weakSelf initializePreview];
                                             [weakSelf.captureSession startRunning];
                                         });
                                     } else {
                                         dispatch_async(dispatch_get_main_queue(), ^{
                                             [weakSelf initializeDeniedPreview];
                                         });
                                     }
                                 }];
        ;
    }
    else if (authStatus == AVAuthorizationStatusAuthorized)
    {
        //доступ дан, понеслось.
        [self initializePreview];
    }
    else //if (authStatus == AVAuthorizationStatusDenied || authStatus == AVAuthorizationStatusRestricted) {
    {
        //доступ запрещён
        [self initializeDeniedPreview];
    }
}


///остановка предпросмотра
- (void) stopPreview {
    [self.view bringSubviewToFront:self.barcodeCapturedImageView];
    [self.view bringSubviewToFront:self.scanButton];
    
    [self.scanButton setTitle:@"СКАНИРОВАТЬ" forState:UIControlStateNormal];
    
    [_captureSession stopRunning];
}

///user GRANTED access to a camera
- (void)initializePreview {
    self.accessDeniedLabel.hidden = YES;
    
    //если нет DevInput - создаём.
    if (!_captureDeviceInput) {
        NSError *error = nil;
        
        _captureDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:_captureDevice error:&error];
        
        if (error) {
            // Handle the error appropriately.
            NSLog(@"ERROR: trying to open camera: %@", error);
        }
    }
    
    if (_captureDeviceInput)
    {
        //input не добавлен и может быть добавлен
        if (!_captureSession.inputs.count
            && [_captureSession canAddInput:_captureDeviceInput])
        {
            [_captureSession addInput:_captureDeviceInput];
        }
    }
    
    if(!_captureStillImageOutput) {
        _captureStillImageOutput = [AVCaptureStillImageOutput new];
        _captureStillImageOutput.outputSettings = @{AVVideoCodecKey : AVVideoCodecJPEG, AVVideoQualityKey : @(1.0) };
        
        if ([_captureSession canAddOutput:_captureStillImageOutput]) {
            [_captureSession addOutput:[self captureStillImageOutput]];
        }
    }
    
        //тосуем view
        [self.view bringSubviewToFront:self.preView];
        [self.view bringSubviewToFront:self.scanButton];
        
        //сеттим тайтл - ? а надо менять ? Скорее нет. При "сканировании" показываем Still image и через секунду переходим на Детали Продукта, где будет происходить загрузка инфы.
//        [self.scanButton setTitle:@"СКАНИРОВАТЬ" forState:UIControlStateNormal];
        
    if (!_captureVideoPreviewLayer) {
        //подвязка AVCaptureVideoPreviewLayer
        _captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:[self captureSession]];
        
        CALayer *captureViewLayer = self.preView.layer;
        
        [_captureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        [_captureVideoPreviewLayer setBounds:[captureViewLayer bounds]];
        [_captureVideoPreviewLayer setFrame:[captureViewLayer bounds]];
        
        [captureViewLayer setMasksToBounds:YES];
        
        if (!captureViewLayer.sublayers.count)
        {
            [captureViewLayer insertSublayer:self.captureVideoPreviewLayer atIndex:0];
        }
    }
}

///user DENIED access to a camera
- (void)initializeDeniedPreview {
    //TODO: серый экран на preView и всё.
    
    [self.preView setBackgroundColor:[UIColor lightGrayColor]];
    [self.view bringSubviewToFront:self.preView];
    [self.view bringSubviewToFront:self.scanButton];
    [self.view bringSubviewToFront:self.accessDeniedLabel];
    
    self.accessDeniedLabel.hidden = NO;

    //TODO: раскомментировать, если отказаться от фейкового сканирования при не данных правах доступа
//    self.scanButton.enabled = NO;
}



#pragma mark - Button Event

- (IBAction)scanButtonTapped:(id)sender {
    
    self.scanButton.userInteractionEnabled = NO;
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if (authStatus == AVAuthorizationStatusAuthorized) {
        //TODO: 1) есть access - сделать фото-синимок, отобразить на self.barcodeCapturedImageView, запустить через секунду пуш с BCProductDetail
        __weak typeof(self) weakSelf = self;
        
        if(![_captureStillImageOutput isCapturingStillImage]) {
            [_captureStillImageOutput
             captureStillImageAsynchronouslyFromConnection:[_captureStillImageOutput connectionWithMediaType:AVMediaTypeVideo]
             completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
                if (imageDataSampleBuffer) {
                    UIImage *capturedStillImage = [[UIImage alloc] initWithData:[AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer]];
                    [weakSelf configureViewWithShotImage:capturedStillImage];
                    [weakSelf recognizeImage:capturedStillImage];
                }
            }];
        }
    } else {
        //TODO: 2) Access DENIED - просто дёргаем "распознавание" и пушим BCProductDetail
        [self recognizeImage:nil];
    }
}

- (void)configureViewWithShotImage:(UIImage *)image {
    //растасовать VIEW, положить image на ту, которая будет показывать
    
    [self.view bringSubviewToFront:self.barcodeCapturedImageView];
    [self.view bringSubviewToFront:self.scanButton];
    
    [self.barcodeCapturedImageView setImage:image];
}

-(void)recognizeImage:(UIImage *)image
{
    NSString * barcode = [[BCRecognizer new] barcodeFromImage:image];
    
    
    
//    //TODO: продёргивание API с коллбэком - заданием Product к BCProductDetailsTCV
//    Product * prod = [Product new];
//    prod.alccode = barcode;
// 
//    details.product = prod;
//    
//    //Пушение контроллера
    
    __weak typeof(self) weakSelf = self;
    
    [[BCService sharedInstance] getAlccodeInfo:barcode
                                  withCallback:^(Product *product) {
                                      [weakSelf pushDetailsWithProduct:product];
                                  }];
    
}

- (void)pushDetailsWithProduct:(Product *)product;
{
    BCProductDetailsTVC *details = [self.storyboard instantiateViewControllerWithIdentifier:@"BCProductDetailsTVC"];
    [details configureWithProduct:product];
    
    __weak typeof(self) weakSelf = self;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf.tabBarController.navigationController pushViewController:details animated:YES];
        
        weakSelf.scanButton.userInteractionEnabled = YES;
    });
    
}

#pragma mark - Text delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];

    BOOL allow = YES;
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if (![myCharSet characterIsMember:c]) {
            allow = NO;
        }
    }
    
    return allow;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    [textField resignFirstResponder];
    
    self.textEntered = [textField.text copy];
    
    //TODO: manual alcocode processing by api
    //создаём контроллер с деталями и пушим
    
    return NO;
}


@end
