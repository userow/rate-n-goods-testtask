//
//  BCProductDetailsTVC.h
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 05/03/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Product;

@interface BCProductDetailsTVC : UITableViewController

@property (nonatomic, strong) Product *product;

- (void)configureWithProduct:(Product *)product;

@end
