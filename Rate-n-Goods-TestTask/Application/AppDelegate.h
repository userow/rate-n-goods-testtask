//
//  AppDelegate.h
//  Rate-n-Goods-TestTask
//
//  Created by Pavel Wasilenko on 24/02/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

