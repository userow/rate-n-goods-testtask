## Rate-n-Goods-TestTask

This repository is a test task repository for Rate-n-Goods.

AVFoundation preview + photo shot.
Simulation of BarCode Recogniser / manual barcode entering
Barcode check at Egaisik.


## Reqirements

xcode 8	
cocoapods 1.+

## Usage

run `pod install` in the project's folder.

## Used Assets

Icons made by [Google](http://www.flaticon.com/authors/google) from [www.flaticon.com](http://www.flaticon.com) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)


Icons made by [Vaadin](http://www.flaticon.com/authors/vaadin) from [www.flaticon.com](http://www.flaticon.com) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)